﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;

namespace WebApplication1.Areas.Identity.Pages.Account
{
	[AllowAnonymous]
	public class RegisterModel : PageModel
	{
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly ILogger<RegisterModel> _logger;
		private readonly IEmailSender _emailSender;
		private readonly IHostingEnvironment _environment;


		public RegisterModel(
			UserManager<ApplicationUser> userManager,
			SignInManager<ApplicationUser> signInManager,
			ILogger<RegisterModel> logger,
			IEmailSender emailSender,
			IHostingEnvironment IHostingEnvironment)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_logger = logger;
			_emailSender = emailSender;
			_environment = IHostingEnvironment;
		}

		[BindProperty]
		public InputModel Input { get; set; }

		public string ReturnUrl { get; set; }

		public class InputModel
		{
			[Required]
			[EmailAddress]
			[Display(Name = "Email")]
			public string Email { get; set; }

			[Required]
			[MinLength(3)]
			[Display(Name = "Name")]
			public string Name { get; set; }

			[Required]
			[StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
			[DataType(DataType.Password)]
			[Display(Name = "Password")]
			public string Password { get; set; }

			[DataType(DataType.Password)]
			[Display(Name = "Confirm password")]
			[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
			public string ConfirmPassword { get; set; }

			[EnumDataType(typeof(Fruit))]
			[Display(Name = "Favourite fruit")]
			[Required(ErrorMessage = "Please chose a fruit, or do you not like fruit???")]
			public Fruit FavouriteFruit { get; set; }

			[Display(Name = "Optional profile picture")]
			[DataType(DataType.Upload)]
			public IFormFile AvatarImage { get; set; }

			[EnumDataType(typeof(ClaimEnum))]
			public ClaimEnum Claim { get; set; }
		}

		public void OnGet(string returnUrl = null)
		{
			ReturnUrl = returnUrl;
		}

		public async Task<IActionResult> OnPostAsync(string returnUrl = null)
		{
			returnUrl = returnUrl ?? Url.Content("~/");
			if (ModelState.IsValid)
			{

				var user = new ApplicationUser
				{
					UserName = Input.Email,
					Email = Input.Email,
					Name = Input.Name,
					FavouriteFruit = Input.FavouriteFruit
				};


				IFormFile file = Input.AvatarImage;
				if (file != null)
				{
					string uploads = Path.Combine(_environment.WebRootPath, "images/avatars");
					string fileId = user.Id.ToString() + Path.GetExtension(file.FileName);
					string filePath = Path.Combine(uploads, fileId);
					FileStream fs = new FileStream(filePath, FileMode.Create);
					await file.CopyToAsync(fs);
					fs.Close();
					user.AvatarString = fileId;
				}


				//This sets the claims for the user.
				//Unless made by an admin, new users can comment
				//If an admin is making a new user, they can chose what to allow from a dropdown lsit
				Claim admin = new Claim("IsAdmin", "true");
				Claim canPost = new Claim("CanPost", "true");
				Claim canComment = new Claim("CanComment", "true");
				Claim restricted = new Claim("CanComment", "false");

				List<Claim> claims = new List<Claim>();

				if (Input.Claim == ClaimEnum.Admin)
				{
					claims.Add(admin);
					claims.Add(canPost);
					claims.Add(canComment);
				}
				else if (Input.Claim == ClaimEnum.Poster)
				{
					claims.Add(canPost);
					claims.Add(canComment);
				}
				else if (Input.Claim == ClaimEnum.Restricted)
				{
					claims.Add(restricted);
				}
				else
				{
					claims.Add(canComment);
				}
				var result = await _userManager.CreateAsync(user, Input.Password);

				if (result.Succeeded)
				{
					_logger.LogInformation("User created a new account with password.");


					await _userManager.AddClaimsAsync(user, claims);


					var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
					var callbackUrl = Url.Page(
						"/Account/ConfirmEmail",
						pageHandler: null,
						values: new { userId = user.Id, code = code },
						protocol: Request.Scheme);

					await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
						$"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

					await _signInManager.SignInAsync(user, isPersistent: false);
					return LocalRedirect(returnUrl);
				}
				foreach (var error in result.Errors)
				{
					ModelState.AddModelError(string.Empty, error.Description);
				}
			}

			// If we got this far, something failed, redisplay form
			return Page();
		}
	}

	//All the types of claims
	public enum ClaimEnum
	{
		Commenter,
		Admin,
		Poster,
		Restricted
	}
}
