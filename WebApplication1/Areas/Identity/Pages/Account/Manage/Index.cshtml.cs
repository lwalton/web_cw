﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApplication1.Models;

namespace WebApplication1.Areas.Identity.Pages.Account.Manage
{
	public partial class IndexModel : PageModel
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly IEmailSender _emailSender;
		private readonly IHostingEnvironment _environment;

		public IndexModel(
			UserManager<ApplicationUser> userManager,
			SignInManager<ApplicationUser> signInManager,
			IEmailSender emailSender,
			IHostingEnvironment IHostingEnvironment)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_emailSender = emailSender;
			_environment = IHostingEnvironment;
		}

		public string Username { get; set; }

		public bool IsEmailConfirmed { get; set; }

		[TempData]
		public string StatusMessage { get; set; }

		[BindProperty]
		public InputModel Input { get; set; }

		public class InputModel
		{
			[Required]
			[EmailAddress]
			public string Email { get; set; }

			[EnumDataType(typeof(Fruit))]
			public Fruit FavouriteFruit { get; set; }

			[Display(Name = "Profile Picture")]
			[DataType(DataType.Upload)]
			public IFormFile AvatarImage { set; get; }

			public string AvatarString { get; set; }

			public String UserClaim { get; set; }
		}

		//GET: Identity/Manage/Index
		public async Task<IActionResult> OnGetAsync()
		{
			ApplicationUser user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
			}

			string userName = await _userManager.GetUserNameAsync(user);
			string email = await _userManager.GetEmailAsync(user);
			string phoneNumber = await _userManager.GetPhoneNumberAsync(user);
			Fruit favouriteFruit = user.FavouriteFruit;
			string avatarString = user.AvatarString;
			string userClaim = await getUserClaimAsync(user);

			Username = userName;

			Input = new InputModel
			{
				Email = email,
				FavouriteFruit = favouriteFruit,
				AvatarImage = null,
				UserClaim = userClaim,
				AvatarString = avatarString
			};

			IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);

			return Page();
		}

		//POST: Identity/Manage/Index
		//Recieves chagnes to a users email or profile image and sets them in the database
		public async Task<IActionResult> OnPostAsync()
		{
			if (!ModelState.IsValid)
			{
				return Page();
			}

			ApplicationUser user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
			}

			Fruit favouriteFruit = user.FavouriteFruit;

			if (Input.FavouriteFruit != favouriteFruit)
			{
				user.FavouriteFruit = Input.FavouriteFruit;
				await _userManager.UpdateAsync(user);
			}

			var email = await _userManager.GetEmailAsync(user);
			if (Input.Email != email)
			{
				var setEmailResult = await _userManager.SetEmailAsync(user, Input.Email);
				if (!setEmailResult.Succeeded)
				{
					var userId = await _userManager.GetUserIdAsync(user);
					throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
				}
			}

			//Gets the uploaded file and sets it as the user's profile picture
			IFormFile file = Input.AvatarImage;
			if (file != null)
			{
				string uploads = Path.Combine(_environment.WebRootPath, "images/avatars");
				string fileId = user.Id.ToString() + Path.GetExtension(file.FileName);
				string filePath = Path.Combine(uploads, fileId);
				FileStream fs = new FileStream(filePath, FileMode.Create);
				await file.CopyToAsync(fs);
				fs.Close();
				user.AvatarString = fileId;
				await _userManager.UpdateAsync(user);
			}

			await _signInManager.RefreshSignInAsync(user);
			StatusMessage = "Your profile has been updated";
			return RedirectToPage();
		}

		//Returns as a string the claims of the user
		private async Task<string> getUserClaimAsync(ApplicationUser u)
		{
			Claim admin = new Claim("IsAdmin", "true");
			var admins = await _userManager.GetUsersForClaimAsync(admin);

			if (admins.Contains(u))
			{
				return "admin";
			}

			Claim poster = new Claim("CanPost", "true");
			var posters = await _userManager.GetUsersForClaimAsync(poster);
			if (posters.Contains(u))
			{
				return "poster";
			}

			Claim userClaim = new Claim("CanComment", "true");
			var users = await _userManager.GetUsersForClaimAsync(userClaim);
			if (users.Contains(u))
			{
				return "user";
			}

			Claim restricted = new Claim("CanComment", "false");
			var restricteds = await _userManager.GetUsersForClaimAsync(restricted);
			if (restricteds.Contains(u))
			{
				return "restricted";
			}

			return "error";
		}
	}
}
