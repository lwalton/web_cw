﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.ViewModels
{
	//View specific model for Posts/Edit
	public class PostEditViewModel
	{
		public string ImageString { get; set; }

		public int PostId { get; set; }

		[Display(Name = "Title")]
		[Required(AllowEmptyStrings = false)]
		[DisplayFormat(ConvertEmptyStringToNull = false)]
		public string PostTitle { get; set; }

		[Display(Name = "Post content")]
		[DataType(DataType.MultilineText)]
		public string PostContent { get; set; }

		//This stores the string of the name of the post image
		[Display(Name = "Add a new image")]
		[DataType(DataType.Upload)]
		public IFormFile PostImage { set; get; }
	}
}
