﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
	//View specific model for the Posts/Details page
	public class PostDetailViewModel
	{
		public Post Post { get; set; }

		public List<Comment> Comments { get; set; }

		public ApplicationUser User { get; set; }

		[Display(Name = "Comment")]
		[Required(ErrorMessage = "You can't submit a blank comment", AllowEmptyStrings = false)]
		public string CommentText { get; set; }

		public string CommentUserName { get; set; }

		public string CommentUserId { get; set; }

		public int CommentPostId { get; set; }

		public string EditCommentId { get; set; }

		public string UserClaim { get; set; }

		//This works out how long ago a post or comment was posted
		public string GetTimeSpan(DateTime startTime)
		{
			TimeSpan difference = DateTime.Now - startTime;
			if (difference.Days > 0)
			{
				if (difference.Days == 1)
				{
					return (difference.Days + " day ago");
				}
				else
				{
					return (difference.Days + " days ago");
				}
			}
			else if (difference.Hours > 0)
			{
				if (difference.Hours == 1)
				{
					return (difference.Hours + " hour ago");
				}
				else
				{
					return (difference.Hours + " hours ago");
				}
			}
			else if (difference.Minutes > 0)
			{
				if (difference.Minutes == 1)
				{
					return (difference.Minutes + " minute ago");
				}
				else
				{
					return (difference.Minutes + " minutes ago");
				}
			}
			else
			{
				if (difference.Seconds == 1)
				{
					return (difference.Seconds + " second ago");
				}
				else
				{
					return (difference.Seconds + " seconds ago");
				}
			}

		}
	}
}
