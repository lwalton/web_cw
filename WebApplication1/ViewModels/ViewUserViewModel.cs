﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
	//View specific model for Home/ViewUsers
	public class ViewUserViewModel
	{
		public List<ApplicationUser> Admins { get; set; }

		public List<ApplicationUser> Posters { get; set; }

		public List<ApplicationUser> Users { get; set; }

		public List<ApplicationUser> Restricteds { get; set; }
	}
}