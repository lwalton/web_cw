﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
	//View specific model for the Post/Create page
	public class PostCreateViewModel
	{
		public ApplicationUser User { get; set; }

		[Display(Name = "Title")]
		[Required]
		public string PostTitle { get; set; }

		[Display(Name = "Post content")]
		[DataType(DataType.MultilineText)]
		public string PostContent { get; set; }

		[Display(Name = "Add an image")]
		[DataType(DataType.Upload)]
		public IFormFile PostImage { set; get; }
	}
}
