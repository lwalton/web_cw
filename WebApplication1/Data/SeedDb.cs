﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Data
{
	public class SeedDb
	{
		public static async Task Initialize(ApplicationDbContext context,
			UserManager<ApplicationUser> userManager,
			RoleManager<IdentityRole> roleManager)
		{
			context.Database.EnsureCreated();
			String member1Email = "Member1@email.com";
			String password = "Password123!";
			await SeedMember(userManager, member1Email, password);
			await SeedUsers(userManager, password);
			await SeedPosts(context, userManager, member1Email);
		}

		//Seeds the first post and comment into their respective databases.
		//Done together as the comment is on the first post
		private static async Task SeedPosts(ApplicationDbContext context, UserManager<ApplicationUser> userManager, string member1Email)
		{
			if (context.Posts.FirstOrDefault() == null)
			{
				ApplicationUser m = await userManager.FindByEmailAsync(member1Email);
				Post testPost = new Post
				{
					Title = "Test Post",
					Content="Welcome to Fruitbook! This is a post that has been seeded into fFruitbook.",
					UserId = m.Id,
					User = m,
					Time = DateTime.Now
				};

				Comment testComment = new Comment
				{
					User = await userManager.FindByEmailAsync(member1Email),
					Text = "Test comment. Good.",
					PostId = testPost.PostId,
					Time = DateTime.Now
				};

				List<Comment> commentsList = new List<Comment>();
				testPost.Comments = commentsList;

				context.Posts.Add(testPost);
				context.Comments.Add(testComment);
				commentsList.Add(testComment);

				context.SaveChanges();
			}
		}

		//Seeds all of the customers into the database
		//To demostrate the different claims I have implemented:
		//Customer1 can post, but is not an administrator,
		//Customer2-Customer4 are regular users that can only comment
		//Customer5 is a restricted user that can only view and can't comment
		private static async Task SeedUsers(UserManager<ApplicationUser> userManager, string password)
		{
			String customer1Email = "Customer1@email.com";
			String customer2Email = "Customer2@email.com";
			String customer3Email = "Customer3@email.com";
			String customer4Email = "Customer4@email.com";
			String customer5Email = "Customer5@email.com";

			Claim canComment = new Claim("CanComment", "true");
			Claim restricted = new Claim("CanComment", "false");
			Claim canPost = new Claim("CanPost", "true");

			Claim[] posters = { canComment, canPost };

			if (await userManager.FindByEmailAsync(customer1Email) == null)
			{
				ApplicationUser customer1 = new ApplicationUser
				{
					UserName = customer1Email,
					Email = customer1Email,
					Name = "Customer1"
				};
				var result = await userManager.CreateAsync(customer1);
				if (result.Succeeded)
				{
					await userManager.AddPasswordAsync(customer1, password);
					await userManager.AddClaimsAsync(customer1, posters);
				}
			}

			if (await userManager.FindByEmailAsync(customer2Email) == null)
			{
				ApplicationUser customer2 = new ApplicationUser
				{
					UserName = customer2Email,
					Email = customer2Email,
					Name = "Customer2"
				};
				var result = await userManager.CreateAsync(customer2);
				if (result.Succeeded)
				{
					await userManager.AddPasswordAsync(customer2, password);
					await userManager.AddClaimAsync(customer2, canComment);
				}
			}

			if (await userManager.FindByEmailAsync(customer3Email) == null)

			{
				ApplicationUser customer3 = new ApplicationUser
				{
					UserName = customer3Email,
					Email = customer3Email,
					Name = "Customer3"
				};
				var result = await userManager.CreateAsync(customer3);
				if (result.Succeeded)
				{
					await userManager.AddPasswordAsync(customer3, password);
					await userManager.AddClaimAsync(customer3, canComment);
				}
			}

			if (await userManager.FindByEmailAsync(customer4Email) == null)
			{
				ApplicationUser customer4 = new ApplicationUser
				{
					UserName = customer4Email,
					Email = customer4Email,
					Name = "Customer4"
				};
				var result = await userManager.CreateAsync(customer4);
				if (result.Succeeded)
				{
					await userManager.AddPasswordAsync(customer4, password);
					await userManager.AddClaimAsync(customer4, canComment);
				}
			}

			if (await userManager.FindByEmailAsync(customer5Email) == null)
			{
				ApplicationUser customer5 = new ApplicationUser
				{
					UserName = customer5Email,
					Email = customer5Email,
					Name = "Customer5"
				};
				var result = await userManager.CreateAsync(customer5);
				if (result.Succeeded)
				{
					await userManager.AddPasswordAsync(customer5, password);
					await userManager.AddClaimAsync(customer5, restricted);
				}
			}
		}

		//Seeds Member1 into the database
		private static async Task SeedMember(UserManager<ApplicationUser> userManager, String member1Email, String password)
		{
			Claim admin = new Claim("IsAdmin", "true");
			Claim userClaim = new Claim("CanComment", "true");
			Claim postClaim = new Claim("CanPost", "true");
			Claim[] claims = { admin, postClaim, userClaim };

			if (await userManager.FindByEmailAsync(member1Email) == null)
			{
				ApplicationUser user = new ApplicationUser
				{
					UserName = member1Email,
					Email = member1Email,
					Name = "Member1"
				};
				var result = await userManager.CreateAsync(user);
				if (result.Succeeded)
				{
					await userManager.AddPasswordAsync(user, password);
					await userManager.AddClaimsAsync(user, claims);
				}
			}
		}

		//Seeds roles into the database. Role based authentication has been superseeded by Claims based authorization
		private static async Task SeedRoles(RoleManager<IdentityRole> roleManager, String password, String role1, String role2)
		{
			if (await roleManager.FindByNameAsync(role1) == null)
			{
				await roleManager.CreateAsync(new IdentityRole(role1));
			}

			if (await roleManager.FindByNameAsync(role2) == null)
			{
				await roleManager.CreateAsync(new IdentityRole(role2));
			}
		}
	}
}
