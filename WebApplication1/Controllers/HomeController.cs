﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Data;
using WebApplication1.Models;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
	public class HomeController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly ApplicationDbContext _context;
		private readonly IHostingEnvironment _environment;

		public HomeController(UserManager<ApplicationUser> userManager,
			ApplicationDbContext context, IHostingEnvironment environment)
		{
			_userManager = userManager;
			_context = context;
			_environment = environment;
		}

		//GET: Home/Index
		public IActionResult Index()
		{
			bool isAuthenticated = User.Identity.IsAuthenticated;
			if (isAuthenticated)
			{
				return RedirectToAction("Index", "Posts");
			}
			else
			{
				return View("NotLoggedIn");
			}

		}

		//GET: Home/About
		public IActionResult About()
		{
			ViewData["Message"] = "This is FruitBook.";

			return View();
		}

		//GET: Home/Contact
		public IActionResult Contact()
		{
			ViewData["Message"] = "We are Fruitbook.";
			ViewData["Email"] = "867775@swansea.ac.uk";

			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		//GET: Home/Error
		public IActionResult Error(string statusCode)
		{
			//This page is returned for any error, including 404
			return View(new ErrorViewModel { ErrorCode = statusCode, RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		//Allows an admin to see all users of the web app, change their privilages and register new users
		//GET: Home/ViewUsers
		[Authorize(Policy = "Admin")]
		public async Task<IActionResult> ViewUsers()
		{
			Claim admin = new Claim("IsAdmin", "true");
			Claim poster = new Claim("CanPost", "true");
			Claim userClaim = new Claim("CanComment", "true");
			Claim restricted = new Claim("CanComment", "false");

			IList<ApplicationUser> admins = await _userManager.GetUsersForClaimAsync(admin);
			IList<ApplicationUser> posters = await _userManager.GetUsersForClaimAsync(poster);
			IList<ApplicationUser> users = await _userManager.GetUsersForClaimAsync(userClaim);
			IList<ApplicationUser> restricteds = await _userManager.GetUsersForClaimAsync(restricted);

			List<ApplicationUser> restrictedsList = restricteds.Except(users).ToList();
			List<ApplicationUser> usersList = users.Except(posters).ToList();
			List<ApplicationUser> postersList = posters.Except(admins).ToList();
			List<ApplicationUser> adminsList = admins.ToList();

			ViewUserViewModel viewModel = new ViewUserViewModel
			{
				Admins = adminsList,
				Posters = postersList,
				Users = usersList,
				Restricteds = restrictedsList
			};

			return (View(viewModel));
		}

		//Allows an admin to delete a user from the webapp
		//GET: Home/DeleteUser/Id
		[Authorize(Policy = "Admin")]
		public async Task<IActionResult> DeleteUser(string Id)
		{
			if (ModelState.IsValid)
			{
				ApplicationUser u = await _userManager.FindByIdAsync(Id);
				if (u != null)
				{
					List<Post> posts = _context.Posts.Where(p => p.UserId == Id).ToList();
					//Delete each image from server
					foreach (Post p in posts)
					{
						DeleteImage(p);
					}
					//Delete the profile image
					DeleteImage(u);
					_context.Posts.RemoveRange(posts);
					_context.Comments.RemoveRange(_context.Comments.Where(c => c.UserId == Id));
					await _userManager.DeleteAsync(u);
				}
			}
			return RedirectToAction("ViewUsers");
		}

		//Allows an admin to upgrade a user's privilages
		//GET: Home/Upgrade/Id
		[Authorize(Policy = "Admin")]
		public async Task<IActionResult> Upgrade(string Id)
		{
			Claim admin = new Claim("IsAdmin", "true");
			Claim poster = new Claim("CanPost", "true");
			Claim userClaim = new Claim("CanComment", "true");
			Claim restricted = new Claim("CanComment", "false");

			IList<ApplicationUser> admins = await _userManager.GetUsersForClaimAsync(admin);
			IList<ApplicationUser> posters = await _userManager.GetUsersForClaimAsync(poster);
			IList<ApplicationUser> users = await _userManager.GetUsersForClaimAsync(userClaim);
			IList<ApplicationUser> restricteds = await _userManager.GetUsersForClaimAsync(restricted);

			List<ApplicationUser> restrictedsList = restricteds.Except(users).ToList();
			List<ApplicationUser> usersList = users.Except(posters).ToList();
			List<ApplicationUser> postersList = posters.Except(admins).ToList();
			List<ApplicationUser> adminsList = admins.ToList();

			if (ModelState.IsValid)
			{
				ApplicationUser u = await _userManager.FindByIdAsync(Id);
				if (u != null)
				{
					if (restrictedsList.Contains(u))
					{
						await _userManager.AddClaimAsync(u, userClaim);
					}
					else if (usersList.Contains(u))
					{
						await _userManager.AddClaimAsync(u, poster);

					}
					else if (postersList.Contains(u))
					{
						await _userManager.AddClaimAsync(u, admin);
					}
					else
					{
						//admin cant be upgraded
						//or something went wrong
					}
				}
			}
			return RedirectToAction("ViewUsers");
		}

		//Allows an admin to downgrade a user's privilages
		//GET: Home/Downgrade/Id
		[Authorize(Policy = "Admin")]
		public async Task<IActionResult> Downgrade(string Id)
		{
			if (ModelState.IsValid)
			{
				Claim admin = new Claim("IsAdmin", "true");
				Claim poster = new Claim("CanPost", "true");
				Claim userClaim = new Claim("CanComment", "true");
				Claim restricted = new Claim("CanComment", "false");

				IList<ApplicationUser> admins = await _userManager.GetUsersForClaimAsync(admin);
				IList<ApplicationUser> posters = await _userManager.GetUsersForClaimAsync(poster);
				IList<ApplicationUser> users = await _userManager.GetUsersForClaimAsync(userClaim);
				IList<ApplicationUser> restricteds = await _userManager.GetUsersForClaimAsync(restricted);

				List<ApplicationUser> restrictedsList = restricteds.Except(users).ToList();
				List<ApplicationUser> usersList = users.Except(posters).ToList();
				List<ApplicationUser> postersList = posters.Except(admins).ToList();
				List<ApplicationUser> adminsList = admins.ToList();

				if (ModelState.IsValid)
				{
					ApplicationUser user = await _userManager.FindByIdAsync(Id);
					if (user != null)
					{
						if (usersList.Contains(user))
						{
							await _userManager.RemoveClaimAsync(user, userClaim);
							await _userManager.AddClaimAsync(user, restricted);
						}
						else if (postersList.Contains(user))
						{
							await _userManager.RemoveClaimAsync(user, poster);

						}
						else if (adminsList.Contains(user))
						{
							await _userManager.RemoveClaimAsync(user, admin);
						}
						else
						{
							//restricted cant be downgraded
							//or something went wrong
						}
					}
				}
			}
			return RedirectToAction("ViewUsers");
		}

		//Allows a logged in user toremove their profile image
		//GET: Home/DeleteProfilePicture
		[Authorize(Policy = "restricted")]
		public async Task<IActionResult> DeleteProfilePicture()
		{
			ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);

			if (user.AvatarString != null)
			{
				//Remove the picture from the server
				DeleteImage(user);
				user.AvatarString = null;
				await _userManager.UpdateAsync(user);
			}

			return RedirectToAction("/Account/Manage/Index", "Identity");
		}

		//Removes the image from a post
		private void DeleteImage(Post post)
		{
			string uploads = Path.Combine(_environment.WebRootPath, "images/posts");
			string path = Path.Combine(uploads, post.ImageString);
			System.IO.File.Delete(path);
		}

		//Removes a user's profile image from the server
		private void DeleteImage(ApplicationUser user)
		{
			if (user.AvatarString != null)
			{
				string uploads = Path.Combine(_environment.WebRootPath, "images\\avatars");
				string oldId = user.AvatarString;
				string oldPath = Path.Combine(uploads, oldId);
				System.IO.File.Delete(oldPath);
			}
		}
	}
}




