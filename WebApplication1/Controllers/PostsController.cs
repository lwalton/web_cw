﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Data;
using WebApplication1.Models;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
	[Authorize(Policy = "Restricted")]
	public class PostsController : Controller
	{
		private readonly ApplicationDbContext _context;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IHostingEnvironment _environment;

		public PostsController(ApplicationDbContext context,
			UserManager<ApplicationUser> userManager,
			IHostingEnvironment IHostingEnvironment)
		{
			_context = context;
			_userManager = userManager;
			_environment = IHostingEnvironment;
		}

		// GET: Posts/Index
		public async Task<IActionResult> Index()
		{
			List<Post> posts = await _context.Posts.ToListAsync();
			List<Post> SortedList = posts.OrderByDescending(o => o.Time).ToList();
			PostIndexViewModel viewModel = await BuildPostIndexViewModel(SortedList);
			return View(viewModel);
		}

		// GET: Posts/Details
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			PostDetailViewModel viewModel = await BuildDetailViewModel(id);

			if (viewModel == null || viewModel.Post == null || viewModel.Post.User == null)
			{
				return NotFound();
			}
			return View(viewModel);
		}

		//POST/Details
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Details(
			[Bind("CommentText,CommentUserName,CommentUserId,CommentPostId")] PostDetailViewModel viewModel)
		{
			if (ModelState.IsValid)
			{
				if (viewModel.CommentText != null)
				{
					await AddComment(viewModel);
				}
			}
			viewModel = await BuildDetailViewModel(viewModel.CommentPostId);

			return View(viewModel);
		}

		// GET: Posts/Create
		[Authorize(Policy = "Poster")]
		public async Task<IActionResult> Create()
		{
			ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
			PostCreateViewModel viewModel = new PostCreateViewModel();
			return View(viewModel);
		}

		//POST: Posts/Create
		[HttpPost]
		[Authorize(Policy = "Poster")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("PostTitle,PostContent,PostImageString")] PostCreateViewModel viewModel, IFormFile file)
		{
			if (ModelState.IsValid)
			{
				ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);

				Post post = new Post
				{
					Title = viewModel.PostTitle,
					Time = DateTime.Now,
					Content = viewModel.PostContent,
					User = user,
					UserId = user.Id,
				};

				_context.Add(post);
				await _context.SaveChangesAsync();

				if (file != null)
				{
					string fileId = await SaveImage(file, post.PostId.ToString());
					post.ImageString = fileId;
					_context.Update(post);
					await _context.SaveChangesAsync();
				}
				return RedirectToAction(nameof(Index));
			}
			else
			{
				return View(viewModel);
			}
		}

		// GET: Posts/Edit
		[Authorize(Policy = "Poster")]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			Post post = await _context.Posts.FindAsync(id);
			if (post == null)
			{
				return NotFound();
			}

			PostEditViewModel viewModel = new PostEditViewModel
			{
				PostId = post.PostId,
				ImageString = post.ImageString,
				PostContent = post.Content,
				PostTitle = post.Title
			};

			return View(viewModel);
		}

		// POST: Posts/Edit
		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize(Policy = "Poster")]
		public async Task<IActionResult> Edit([Bind("PostTitle,PostContent,ImageString, PostId")] PostEditViewModel viewModel, IFormFile file)
		{
			Post post = await _context.Posts.FindAsync(viewModel.PostId);

			if (ModelState.IsValid)
			{
				try
				{
					if (post != null)
					{
						post.Title = viewModel.PostTitle;
						if (viewModel.PostContent == null)
						{
							//Content can be empty, but not null
							viewModel.PostContent = "";
						}
						post.Content = viewModel.PostContent;
						_context.Update(post);
						await _context.SaveChangesAsync();
						if (file != null)
						{

							if (post.ImageString != null)
							{
								DeletePostImage(post);
							}
							post.ImageString = await SaveImage(file, post.PostId.ToString());
							viewModel.ImageString = post.ImageString;
							_context.Update(post);
							await _context.SaveChangesAsync();
							return View(viewModel);
						}
						else if (viewModel.ImageString == null && post.ImageString != null)
						{
							DeletePostImage(post);
							post.ImageString = null;
							_context.Update(post);
							await _context.SaveChangesAsync();
							return View(viewModel);
						}
					}

				}
				catch (DbUpdateConcurrencyException)
				{
					if (!PostExists(viewModel.PostId))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				//Returns user to the details of their new post
				return RedirectToAction("Details/" + viewModel.PostId);
			}
			return View(viewModel);
		}

		// GET: Posts/Delete
		[Authorize(Policy = "Poster")]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			Post post = await _context.Posts
				.FirstOrDefaultAsync(m => m.PostId == id);
			if (post == null)
			{
				return NotFound();
			}

			return View(post);
		}

		// POST: Posts/Delete
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		[Authorize(Policy = "Poster")]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			Post post = await _context.Posts.FindAsync(id);
			if (post.ImageString != null)
			{
				DeletePostImage(post);
			}
			_context.Posts.Remove(post);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		// GET: Posts/DeleteComment
		[Authorize(Policy = "User")]
		public async Task<IActionResult> DeleteComment(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			Comment comment = await _context.Comments
				.FirstOrDefaultAsync(m => m.CommentId == id);
			if (comment == null)
			{
				return NotFound();
			}

			int postId = comment.PostId;

			_context.Comments.Remove(comment);
			await _context.SaveChangesAsync();
			return RedirectToAction("Details/" + postId);
		}

		// POST: Posts/EditComment
		[HttpPost, ActionName("EditComment")]
		[ValidateAntiForgeryToken]
		[Authorize(Policy = "User")]
		public async Task<IActionResult> EditComment([Bind("EditCommentId, CommentText, CommentPostId")] PostIndexViewModel viewModel)
		{
			Comment comment = await _context.Comments.FindAsync(viewModel.EditCommentId);
			if (viewModel.CommentText != null)
			{
				comment.Text = viewModel.CommentText;
				await _context.SaveChangesAsync();
			}

			string postId = viewModel.CommentPostId.ToString();
			return RedirectToAction("Details/" + postId + "#comments");
		}

		//Builds PostIndexViewModel
		private async Task<PostIndexViewModel> BuildPostIndexViewModel(List<Post> SortedList)
		{
			//I had previously used a foreach loop, but research shows that a for loop is less computationally expensive
			for (int i = 0; i < SortedList.Count; i++)
			{
				Post post = SortedList[i];
				//Need to load user and comment data into each post manually
				//They are null by deafult which I think is due to lazy loading
				post.User = await _userManager.FindByIdAsync(post.UserId);
				post.Comments = _context.Comments.Where(d => d.PostId == post.PostId).ToList();
				foreach (Comment comment in post.Comments)
				{
					comment.User = await _userManager.FindByIdAsync(comment.UserId);
				}
			}

			ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);

			PostIndexViewModel viewModel = new PostIndexViewModel
			{
				User = user,
				Posts = SortedList,
				UserClaim = await GetUserClaimAsync(user)
			};
			return viewModel;
		}

		//Saves an uploaded IFormFile to the server for a post image
		private async Task<string> SaveImage(IFormFile file, string postId)
		{
			string uploads = Path.Combine(_environment.WebRootPath, "images/posts");
			string fileId = postId + Path.GetExtension(file.FileName);
			string filePath = Path.Combine(uploads, fileId);
			FileStream fs = new FileStream(filePath, FileMode.Create);
			await file.CopyToAsync(fs);
			fs.Close();
			return fileId;
		}

		//Deletes an image from a post
		private void DeletePostImage(Post post)
		{
			string uploads = Path.Combine(_environment.WebRootPath, "images/posts");
			string path = Path.Combine(uploads, post.ImageString);
			System.IO.File.Delete(path);
		}

		//Checks if a post exists
		private bool PostExists(int id)
		{
			return _context.Posts.Any(e => e.PostId == id);
		}

		//Builds the ViewModel for viewing the details of a post
		private async Task<PostDetailViewModel> BuildDetailViewModel(int? id)
		{
			Post post = await _context.Posts.FirstOrDefaultAsync(m => m.PostId == id);

			if (post != null)
			{
				PostDetailViewModel viewModel = new PostDetailViewModel
				{
					Post = post
				};
				viewModel.Post.User = await _userManager.FindByIdAsync(viewModel.Post.UserId);
				viewModel.Comments = await _context.Comments.Where(m => m.PostId == id).ToListAsync();
				foreach (Comment c in viewModel.Comments)
				{
					c.User = await _userManager.FindByIdAsync(c.UserId);
				}

				ApplicationUser u = await _userManager.GetUserAsync(HttpContext.User);
				viewModel.User = u;
				viewModel.UserClaim = await GetUserClaimAsync(u);
				return viewModel;
			}
			return null;
		}

		//Gets the claims of a User u
		private async Task<string> GetUserClaimAsync(ApplicationUser u)
		{
			Claim admin = new Claim("IsAdmin", "true");
			IList<ApplicationUser> admins = await _userManager.GetUsersForClaimAsync(admin);

			if (admins.Contains(u))
			{
				return "admin";
			}

			Claim poster = new Claim("CanPost", "true");
			IList<ApplicationUser> posters = await _userManager.GetUsersForClaimAsync(poster);
			if (posters.Contains(u))
			{
				return "poster";
			}

			Claim userClaim = new Claim("CanComment", "true");
			IList<ApplicationUser> users = await _userManager.GetUsersForClaimAsync(userClaim);
			if (users.Contains(u))
			{
				return "user";
			}

			Claim restricted = new Claim("CanComment", "false");
			IList<ApplicationUser> restricteds = await _userManager.GetUsersForClaimAsync(restricted);
			if (restricteds.Contains(u))
			{
				return "restricted";
			}

			return "error";
		}

		//Adds a comment to the database fomr a PostDetailViewModel
		private async Task AddComment(PostDetailViewModel viewModel)
		{
			Comment c = new Comment
			{
				Text = viewModel.CommentText,
				PostId = viewModel.CommentPostId,
				UserId = viewModel.CommentUserId,
				Time = DateTime.Now
			};
			_context.Add(c);
			await _context.SaveChangesAsync();

		}
	}
}

