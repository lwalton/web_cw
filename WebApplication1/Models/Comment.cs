﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
	//Model for a comment on Fruitbook
	public class Comment
	{
		[Required]
		[Key]
		public int CommentId { get; set; }

		[Display(Name ="Comment text")]
		[Required(ErrorMessage = "You can't submit a blank comment", AllowEmptyStrings = false)]
		public string Text { get; set; }

		[Required]
		public ApplicationUser User { get; set; }

		[Required]
		[ForeignKey("UserId")]
		public string UserId { get; set; }

		[ForeignKey("PostId")]
		[Required]
		public int PostId { get; set; }

		[Required]
		public DateTime Time { get; set; }
	}
}
