﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
	//Model for a user of FruitBook
	public class ApplicationUser : IdentityUser
	{
		public ApplicationUser() : base() { }

		[Required]
		public String Name { get; set; }

		[EnumDataType(typeof(Fruit))]
		[Display(Name = "Favourite fruit")]
		[Required(ErrorMessage = "Please chose a fruit, or do you not like fruit???")]
		public Fruit FavouriteFruit { get; set; }

		//This will be a string of the file name of the users profile picture
		public string AvatarString { get; set; }
	}

	//Every user must have a favourite fruit from this list
	public enum Fruit
	{
		Nothing,
		Apple,
		Apricot,
		Avocado,
		Banana,
		Blackberry,
		Blackcurrant,
		Blueberry,
		Boysenberry,
		Breadfruit,
		Cantaloupe,
		Cherimoya,
		Cherry,
		Clementine,
		Cloudberry,
		Coconut,
		Cranberry,
		Cucumber,
		Currant,
		Damson,
		Date,
		Dragonfruit,
		Durian,
		Eggplant,
		Elderberry,
		Feijoa,
		Fig,
		Gooseberry,
		Grape,
		Grapefruit,
		Guava,
		Honeydew,
		Huckleberry,
		Jackfruit,
		Jambul,
		Jujube,
		Kiwi,
		Kumquat,
		Lemon,
		Lime,
		Loquat,
		Lychee,
		Mandarine,
		Mango,
		Mulberry,
		Nectarine,
		Orange,
		Pamelo,
		Papaya,
		Passionfruit,
		Peach,
		Pear,
		Persimmon,
		Physalis,
		Pineapple,
		Plum,
		Pomegranate,
		Pomelo,
		Quince,
		Raisin,
		Rambutan,
		Raspberry,
		Redcurrant,
		Satsuma,
		Starfruit,
		Strawberry,
		Tamarillo,
		Tangerine,
		Tomato,
		Uglifruit,
		Watermelon
	}

	//Role based authoriztion has been replaced by claim based authorization
	//public enum Role
	//{
	//	Admin,
	//	User
	//}
}
