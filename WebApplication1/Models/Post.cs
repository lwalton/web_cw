﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
	//Model for a Post
	public class Post
	{
		[Required]
		[Key]
		public int PostId { get; set; }

		[Required]
		[Display(Name = "Title")]
		[DataType(DataType.Text)]
		public string Title { get; set; }

		[Required]
		public DateTime Time { get; set; }

		[DataType(DataType.Upload)]
		[Display(Name = "Upload image")]
		//This will be a string of the file name of the post's picture
		public string ImageString { get; set; }

		[Display(Name = "Post content")]
		[DataType(DataType.MultilineText)]
		public string Content { get; set; }

		[Required]
		[ForeignKey("UserId")]
		public string UserId { get; set; }

		[Required]
		public ApplicationUser User { get; set; }

		public virtual List<Comment> Comments { get; set; }
	}
}
